# to backup, run as root:
sudo ETCDCTL_API=3  etcdctl snapshot save snapshot.db   --cacert /etc/kubernetes/pki/etcd/ca.crt   --cert /etc/kubernetes/pki/etcd/server.crt   --key /etc/kubernetes/pki/etcd/server.key
aws s3 cp snapshot.db  s3://leetcyber/snapshot.db


# to restore
#ETCDCTL_API=3 etcdctl  --data-dir /var/lib/etcd-from-backup snapshot restore /opt/snapshot-pre-boot.db
